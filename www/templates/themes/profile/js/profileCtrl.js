// Author: Daniel Castillo
appControllers.controller('profileCtrl', function ($scope, $rootScope, $state, AppUtil, localStorage, AuthService, ParseError, RestService, $cordovaImagePicker ) {

	// Validations
	if (!$rootScope.currentUser) {
		$state.go('app.login');
		return;
	}


	/**
	* Función para cambiar la foto de perfil
	*/
	$scope.change_picture = function () {
		// Set options for select image from mobile gallery.
        var options = {
            maximumImagesCount: 1,
            width: 300,
            height: 300,
            quality: 90
        }; // End Set options.

        // select image by calling $cordovaImagePicker.getPictures(options)
        $cordovaImagePicker.getPictures(options)

            .then(function (results) {

            	var params = new Object();
		        params.appId = appOptions.appId;
		        params.custom_name = 'profile_' + $rootScope.currentUser.objectId + '.jpg';
		        params.type = 'operator';

                // Parámetro de subida de imagen
                var fileURL = results[0];
                var options = new FileUploadOptions();
                options.fileKey = "file";
                options.fileName = fileURL.substr(fileURL.lastIndexOf('/') + 1);
                options.mimeType = "image/jpg";
                options.headers = { Connection: "close" };
                options.chunkedMode = false;

                // Se sube la imagen
                WebService.show_loading();
                var ft = new FileTransfer();
                ft.upload(fileURL, encodeURI(appOptions.externalUrl + 'includes/upload.php?appId='+ params.appId +'&custom_name='+ params.custom_name +'&type='+ params.type), function (success) {

                	var image = document.getElementById('profilePicture');
                    image.src = imageURI;
                    $ionicLoading.hide();

                }, function (e) {
                    $ionicPopup.alert({ title: 'Error', template: 'Ha ocurrido un error al subir la imagen' });
                    $ionicLoading.hide();
                }, options);

            }, function (error) {
                console.log(error);
                $ionicPopup.alert({ title: 'Error', template: 'Ha ocurrido un error en la elección de la imagen' });
            });
	}

}); // End of controller.