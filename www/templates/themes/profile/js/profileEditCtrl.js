// Author: Daniel Castillo
appControllers.controller('profileEditCtrl', function ($scope, $rootScope, $state, AppUtil, localStorage, AuthService, ParseError, RestService, $http ) {

	// Validations
	if (!$rootScope.currentUser) {
		$state.go('app.login');
		return;
	}

	// Model
	$scope.changePassword = {};
	$scope.states = [
        { name: 'Aguascaliente' },
        { name: 'Baja Californa' },
        { name: 'Baja California Sur' },
        { name: 'Campeche' },
        { name: 'Chiapas' },
        { name: 'Chihuahua' },
        { name: 'Coahuila' },
        { name: 'Colima' },
        { name: 'DF' },
        { name: 'Durango' },
        { name: 'Guanajuato' },
        { name: 'Guerrero' },
        { name: 'Hidalgo' },
        { name: 'Jalisco' },
        { name: 'Mexico' },
        { name: 'Michoacan' },
        { name: 'Morelos' },
        { name: 'Nayarit' },
        { name: 'Nuevo Leon' },
        { name: 'Oaxaca' },
        { name: 'Puebla' },
        { name: 'Queretero' },
        { name: 'Quintana Roo' },
        { name: 'San Luis Potosi' },
        { name: 'Sinaloa' },
        { name: 'Sonora' },
        { name: 'Tabasco' },
        { name: 'Tamaulipas' },
        { name: 'Tlaxcala' },
        { name: 'Veracruz' },
        { name: 'Yucatan' }
    ];

	/**
	* Función para actualizar la información personal del usuario
	*/
	$scope.update_info = function () {

		if (!$rootScope.currentUser.first_name) {
			AppUtil.showToast('bottom', 'Ingrese su(s) nombre(s).');
			return;
		}
		if (!$rootScope.currentUser.last_name) {
			AppUtil.showToast('bottom', 'Ingrese su(s) apellido(s).');
			return;
		}

		if ($rootScope.currentUser.type == 'doctor') {
			if (!$rootScope.currentUser.email) {
				AppUtil.showToast('bottom', 'Ingrese un correo eletrónico válido.');
				return;
			}
			if (!$rootScope.currentUser.speciality) {
				AppUtil.showToast('bottom', 'Ingrese su especialidad.');
				return;
			}
			if (!$rootScope.currentUser.identification) {
				AppUtil.showToast('bottom', 'Ingrese su cédula profesional.');
				return;
			}
			if (!$rootScope.currentUser.position) {
				AppUtil.showToast('bottom', 'Ingrese su cargo en el hospital.');
				return;
			}
		}

		$rootScope.showSpinner = true;
		$rootScope.currentUser = _.omit($rootScope.currentUser, ['password', 'createdAt', 'updatedAt']);

		$http.defaults.headers.common['X-Parse-Session-Token'] = $rootScope.currentUser.sessionToken;
		RestService.update($rootScope.currentUser, function (resp) {
			delete $http.defaults.headers.common['X-Parse-Session-Token'];

			_.extend($rootScope.currentUser, resp);
			localStorage.set('currentUser', $rootScope.currentUser);

			AppUtil.showToast('bottom', 'Se ha actualizado su información.');
			$rootScope.showSpinner = false;
		});
	}


	/**
	* Función para cambiar la contraseña
	*/
	$scope.update_password = function () {

		// Validaciones
		if (!$scope.changePassword.password) {
			AppUtil.showToast('bottom', 'Ingrese su contraseña actual.');
			return;
		}

		if (!$scope.changePassword.newpassword) {
			AppUtil.showToast('bottom', 'Ingrese su nueva contraseña.');
			return;
		}

		if ($scope.changePassword.newpassword && $scope.changePassword.newpassword.length < 6) {
			AppUtil.showToast('bottom', 'La nueva contraseña debe tener mínimo 6 caracteres.');
			return;
		}

		if (!$scope.changePassword.repassword || $scope.changePassword.newpassword != $scope.changePassword.repassword) {
			AppUtil.showToast('bottom', 'Verifique que la confirmación de contraseña sea igual.');
			return;
		}

		$rootScope.showSpinner = true;
		AuthService.auth().login({
            username: $rootScope.currentUser.username,
            password: $scope.changePassword.password
        }, function (resp) {

        	AppUtil.logoutUser(resp.sessionToken);

            AppUtil.resetUserPassword($rootScope.currentUser.objectId, $scope.changePassword.newpassword, function (resp) {

            	var ciphertext = CryptoJS.AES.encrypt($scope.changePassword.newpassword, 'siys');
				$rootScope.currentUser.encrypt = ciphertext.toString();
				
				$http.defaults.headers.common['X-Parse-Session-Token'] = $rootScope.currentUser.sessionToken;
				RestService.update($rootScope.currentUser, function (resp) {
					delete $http.defaults.headers.common['X-Parse-Session-Token'];

					$scope.changePassword = {};
					AppUtil.showToast('bottom', 'Se ha actualizado su contraseña.');
					$rootScope.showSpinner = false;
				});
			});

        }, function (e) {
            console.log('err', e);
            
            // Muestra mensaje de error.
            AppUtil.showToast('bottom', 'Su contraseña actual es incorrecta');
            $rootScope.showSpinner = false;
        });
	}

}); // End of controller.