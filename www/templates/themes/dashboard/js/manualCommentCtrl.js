appControllers.controller('manualCommentCtrl', function ($scope, $rootScope, $state, $stateParams, AppUtil, localStorage, AuthService, ParseError, RestService, $http, appOptions, $ionicScrollDelegate ) {

	// Models
	var limit = 10;
	$scope.skip = 0;
	$scope.total_comments = 0;
	$scope.noMoreItemsAvailable = false;
	$scope.hideMessage = true;
	$scope.comments = [];


	/**
	* Consulta la info del manual
	*/
	RestService.get({clase: 'Manual', objectId: $stateParams.manualId}, function (resp) {
		$scope.manual = resp;
	});


	/**
	* Obtiene los registros
	*/
	$scope.loadData = function () {

		RestService.get({
			clase: 'Comment',
			where: {
				manual: {__type: 'Pointer', className: 'Manual', objectId: $stateParams.manualId}
			},
			include: 'user',
			limit: limit,
			skip: $scope.skip,
			order: '-createdAt',
			count: 1
		}, function (resp) {
			$scope.total_comments = resp.count;
			$scope.comments = $scope.comments.concat(resp.results);

			$scope.skip = $scope.skip + limit;
			$scope.$broadcast('scroll.infiniteScrollComplete');
			$scope.hideMessage = false;
		});

		// Validación para que el infinite scroll no cargue si no hay más registros que consultar
		if ($scope.skip > $scope.total_comments) {
			$scope.noMoreItemsAvailable = true;
		}
	}


	/**
	* Agregar comentario
	*/
	$scope.add_comment = function () {
		
		// Model
		var comment = {};
		comment.clase = 'Comment';
		comment.text = $scope.new_comment;
		comment.user = {__type: 'Pointer', className: '_User', objectId: $rootScope.currentUser.objectId};
		comment.manual = {__type: 'Pointer', className: 'Manual', objectId: $stateParams.manualId};
		comment.likes = 0;

		$rootScope.showSpinner = true;
		RestService.create(comment, function (resp) {
			$rootScope.showSpinner = false;

			if (!$scope.comments)
				$scope.comments = [];

			_.extend(comment, resp);
			comment.user = $rootScope.currentUser;
			$scope.comments.unshift(comment);

			$scope.new_comment = '';
			$ionicScrollDelegate.scrollTop();

			RestService.update({
				clase: 'Manual', 
				objectId: $stateParams.manualId, 
				comments_quantity: $scope.manual.comments_quantity + 1
			}, function () {});
		});
	}


	/**
	* Like al comentario
	*/
	$scope.like_comment = function (id) {

		var obj = _.find($scope.comments, {objectId: id});

		$rootScope.showSpinner = true;
		RestService.get({
			clase: 'Like',
			where: {
				user: {__type: 'Pointer', className: '_User', objectId: $rootScope.currentUser.objectId},
				comment: {__type: 'Pointer', className: 'Comment', objectId: id}
			}
		}, function (resp) {

			$rootScope.showSpinner = false;

			// Si el usuario actual todavía no ha dado like al comentario
			if (resp.results.length == 0) {
				_.extend(obj, {likes: obj.likes + 1});

				RestService.update({clase: 'Comment', objectId: id, likes: obj.likes}, function () {

					// Model
					var like = {};
					like.clase = 'Like';
					like.user = {__type: 'Pointer', className: '_User', objectId: $rootScope.currentUser.objectId};
					like.comment = {__type: 'Pointer', className: 'Comment', objectId: id};

					// Se crea el registro del like
					RestService.create(like);
				});
			}
		});
	}

}); // End of controller.