appControllers.controller('manualDetailCtrl', function ($scope, $rootScope, $state, $stateParams, AppUtil, localStorage, AuthService, ParseError, RestService, $http, appOptions ) {

	$rootScope.showSpinner = true;
	RestService.get({clase: 'Manual', objectId: $stateParams.manualId}, function (resp) {
		$scope.manual = resp;
		$rootScope.showSpinner = false;
	});


	/**
    * Función para obtener la extensión de un archivo
    */
    $scope.get_extension = function (filename) {
        return /[^.]+$/.exec(filename.toLowerCase())[0];
    }

}); // End of controller.