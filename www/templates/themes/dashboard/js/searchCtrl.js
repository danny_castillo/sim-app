appControllers.controller('searchCtrl', function ($scope, $rootScope, $state, $stateParams, AppUtil, localStorage, AuthService, ParseError, RestService, $http, appOptions ) {

	// Models
	var limit = 100;
	$scope.skip = 0;
	$scope.total_manuals_searched = 0;
	$scope.noMoreItemsAvailable = false;
	$scope.hideMessage = true;
	$scope.search = '';


	/**
	* Obtiene la colección de productos
	*/
	$scope.loadData = function () {

		var params = {};

		// Parámetros
		_.extend(params, {
			name: {$regex: '(?i)' + $stateParams.string}
		});

		RestService.get({clase: 'Manual', 
			where: params,
			limit: limit,
			skip: $scope.skip,
			order: '-createdAt',
			count: 1
		}, function (resp) {
			$scope.total_manuals_searched = resp.count;

			// Creamos las colecciones nuevas (global y por sección)
			if (!$scope.manuals_searched)
				$scope.manuals_searched = [];

			$scope.manuals_searched = $scope.manuals_searched.concat(resp.results);

			$scope.skip = $scope.skip + limit;
			$scope.$broadcast('scroll.infiniteScrollComplete');
			$scope.hideMessage = false;
		});

		// Validación para que el infinite scroll no cargue si no hay más registros que consultar
		if ($scope.skip > $scope.total_manuals_searched) {
			$scope.noMoreItemsAvailable = true;
		}
	}

}); // End of controller.