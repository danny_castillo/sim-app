appControllers.controller('videoCtrl', function ($scope, $rootScope, $state, $stateParams, AppUtil, localStorage, AuthService, ParseError, RestService, $http, appOptions ) {

	// Models
	var limit = 10;
	$scope.skip = 0;
	$scope.total_videos = 0;
	$scope.noMoreItemsAvailable = false;
	$scope.hideMessage = true;
	$scope.search = '';
	$rootScope.videos = [];


	/**
	* Función que lleva a los resultados de búsqueda
	*/
	$scope.show_searched = function (string) {
		$state.go('app.search', {string: string});
		$scope.search = '';
	}


	/**
	* Obtiene la colección de productos
	*/
	$scope.loadData = function () {

		RestService.get({clase: 'Manual', 
			where: {type: 'video'},
			limit: limit,
			skip: $scope.skip,
			order: '-createdAt',
			count: 1
		}, function (resp) {
			$scope.total_videos = resp.count;
			$rootScope.videos = $rootScope.videos.concat(resp.results);

			$scope.skip = $scope.skip + limit;
			$scope.$broadcast('scroll.infiniteScrollComplete');
			$scope.hideMessage = false;
		});

		// Validación para que el infinite scroll no cargue si no hay más registros que consultar
		if ($scope.skip > $scope.total_videos) {
			$scope.noMoreItemsAvailable = true;
		}
	}


	/**
	* Refresh
	*/
	$scope.doRefresh = function () {
		// Models
		var limit = 10;
		$scope.skip = 0;
		$scope.total_manuals = 0;
		$scope.noMoreItemsAvailable = false;
		$scope.hideMessage = true;
		$rootScope.videos = [];

		$scope.loadData();
	}

}); // End of controller.