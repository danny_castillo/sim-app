// Controller of login to App
// Author: Daniel Castillo
appControllers.controller('loginCtrl', function ($scope, $rootScope, $state, AppUtil, localStorage, AuthService, ParseError, RestService, $ionicHistory) {

    // Session validation
    if ($rootScope.currentUser) {
        $state.go('app.dashboard');
    }

    // Modelos
    $scope.user = {username: '', password: ''};

	$scope.login = function () {

        if (!$scope.user.username) {
            AppUtil.showToast('bottom', 'Ingrese su nombre de usuario.');
            return;
        }
        if (!$scope.user.password) {
            AppUtil.showToast('bottom', 'Ingrese su contraseña.');
            return;
        }

        $rootScope.showSpinner = true;

        // Login
        AuthService.auth().login({
            username: $scope.user.username,
            password: $scope.user.password
        }, function (resp) {

            // Model
            $rootScope.currentUser = resp;
            localStorage.set('currentUser', $rootScope.currentUser);

            $rootScope.showSpinner = false;
            $state.go('app.dashboard');

        }, function (e) {
            console.log('err', e);
            
            // Muestra mensaje de error.
            $rootScope.showSpinner = false;
            AppUtil.showToast('bottom', ParseError.message(e.data.code));
            return;
        });
    };
}); // End of controller.