// Controller of sign up to App
// Author: Daniel Castillo
appControllers.controller('signUpCtrl', function ($scope, $rootScope, $state, AppUtil, RestService, ParseError, localStorage, Base64, $http, $ionicHistory, OpenPayService) {
    
    if ($rootScope.currentUser) {
        $state.go('app.dashboard');
    }


	// Modelo 
	$scope.user = {};


	/**
	* Función para registrar un usuario demo
	*/
	$scope.signUp = function () {

		// Validaciones
		if (!$scope.user.username) {
			AppUtil.showToast('bottom', 'Ingrese su nombre de usuario.');
			return;
		}

		if (!$scope.user.first_name) {
			AppUtil.showToast('bottom', 'Ingrese su(s) nombre(s).');
			return;
		}

		if (!$scope.user.last_name) {
			AppUtil.showToast('bottom', 'Ingrese su(s) apellido(s).');
			return;
		}

		if (!$scope.user.password) {
			AppUtil.showToast('bottom', 'Ingrese su contraseña.');
			return;
		}

		if ($scope.user.password && $scope.user.password.length < 6) {
			AppUtil.showToast('bottom', 'La contraseña debe tener mínimo 6 caracteres.');
			return;
		}

		if (!$scope.user.repassword || $scope.user.password != $scope.user.repassword) {
			AppUtil.showToast('bottom', 'Repita la misma contraseña.');
			return;
		}

		if (!$scope.user.phone) {
			AppUtil.showToast('bottom', 'Ingrese un número telefónico.');
			return;
		}

		if (!$scope.user.email) {
			AppUtil.showToast('bottom', 'Ingrese un correo eletrónico válido.');
			return;
		}

		RestService.get({clase: '_User', where: {username: $scope.user.username}}, function (resp) {

			if (resp.results.length > 0) {
				AppUtil.showToast('bottom', 'El nombre de usuario ya se encuentra registrado.');
				return;

			} else {
				RestService.get({clase: '_User', where: {email: $scope.user.email}}, function (resp) {
					if (resp.results.length > 0) {
						AppUtil.showToast('bottom', 'El correo eletrónico ya se encuentra registrado.');
						return;

					} else {

						// Modelo
						var user = _($scope.user).clone();
						user.clase = '_User';
						user.active = true;
						user.type = 'user';
						var ciphertext = CryptoJS.AES.encrypt(user.password, 'shopple');
						user.encrypt = ciphertext.toString();
						user = _.omit(user, ['repassword']);

						if(user.phone)
                			user.phone = user.phone.replace(/[^\d.]/g, '');

						$rootScope.showSpinner = true;
						RestService.create(user, function (resp) {

							_.extend(user, resp);
							$rootScope.currentUser = user;
							localStorage.set('currentUser', $rootScope.currentUser);

							// Creamos el cliente en Open Pay
							OpenPayService.post('customers', {
								external_id: $rootScope.currentUser.objectId,
								name: $rootScope.currentUser.first_name,
								last_name: $rootScope.currentUser.last_name,
								email: $rootScope.currentUser.email,
								phone_number: $rootScope.currentUser.phone,
								requires_account: false
						    }, function (resp) {

						    	$http.defaults.headers.common['X-Parse-Session-Token'] = $rootScope.currentUser.sessionToken;
						    	RestService.update({clase: '_User', objectId: $rootScope.currentUser.objectId, openPayId: resp.id}, function () {
						    		delete $http.defaults.headers.common['X-Parse-Session-Token'];
						    		// Guardamos en local
						    		$rootScope.currentUser.openPayId = resp.id;
									localStorage.set('currentUser', $rootScope.currentUser);
								  
									AppUtil.showToast('bottom', 'Se ha registrado con éxito.');
									$ionicHistory.goBack(); 
									$rootScope.showSpinner = false;
						    	});
						    });						

						}, function (e) {
							console.log('err', e);
							AppUtil.showToast('bottom', ParseError.message(e.data.code));
							$rootScope.showSpinner = false;
						});
					}
				});
			}
		});
	}

}); // End of controller.