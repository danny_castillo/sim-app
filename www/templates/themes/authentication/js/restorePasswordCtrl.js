// Controller of login to App
// Author: Daniel Castillo
appControllers.controller('restorePasswordCtrl', function ($scope, $rootScope, $state, AppUtil, localStorage, AuthService, ParseError, RestService, $http, appOptions) {

    // Modelos
    $scope.user = {email: '',};

    $scope.restore_password = function () {

        if (!$scope.user.email) {
            AppUtil.noty('warning', 'Ingrese su correo electrónico válido');
            return;
        }

        $rootScope.showSpinner();

        // Client Login
        $http.defaults.headers.common['X-Parse-Master-Key'] = appOptions.masterKey;
        RestService.get({clase: '_User', 
            where: {
                $or: [
                    {email: $scope.user.email}, 
                    {username: $scope.user.email}
                ]
            }
        }, function (resp) {
            delete $http.defaults.headers.common['X-Parse-Master-Key'];

            if (resp.results.length == 0) {
                AppUtil.noty('error', 'El correo electrónico que ingresó es incorrecto');
                $rootScope.hideSpinner();
                return;
            }

            // Decrypt 
            resp = resp.results[0];
            var bytes  = CryptoJS.AES.decrypt(resp.encrypt, 'shopple');
            resp.encrypt = bytes.toString(CryptoJS.enc.Utf8);

            // Model
            AppUtil.send_mail({
                target: 'reset_password',
                email: resp.email, 
                string: resp.encrypt, 
                first_name: resp.first_name, 
                last_name: resp.last_name
            }, function () {
                AppUtil.noty('success', 'Le hemos enviado su contraseña al correo electrónico');
                $rootScope.hideSpinner();
            });

        }, function (e) {
            console.log('err', e);
            
            // Muestra mensaje de error.
            AppUtil.noty('error', ParseError.message(e.data.code));
            $rootScope.hideSpinner();
        });
    };

}); // End of controller.