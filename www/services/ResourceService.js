'use strict';

appControllers.factory('ResourceService', function ($http, $resource, appOptions, $rootScope) {
	
	// Devuelve una coleccion de servicios.
	return {

		// Llamada basica.
		call: function (url, params, methods) {

			// Permite conectarse fuera del dominio.
			//$http.defaults.useXDomains = true;
			delete $http.defaults.headers.common['X-Requested-With'];
			delete $http.defaults.headers.common['X-Parse-Session-Token'];

			$http.defaults.headers.common['X-Parse-Application-Id'] = appOptions.appId;
			$http.defaults.headers.common['X-Parse-REST-API-Key'] = appOptions.apiKey;

			// Extiende los metodos si es necesario.
			var defaultMethods = {
				all: { method: 'GET', isArray: true },
				get: { method: 'GET' },
				create: { method: 'POST' },
				update: { method: 'PUT' },
				delete: { method: 'DELETE' },
				poster: { method: 'POST' },
				posterArray: { method: 'POST', isArray: true },
			};
			methods = angular.extend(defaultMethods, methods);

			// Extiende los parametros de entrada si es necesario.
			var defaultParams = {};
			params = angular.extend(defaultParams, params);

			// Objeto de recurso.
			var resource = $resource(appOptions.url+url, params, methods);
			
			// Se extiende el objeto para el guardado automatico.
			resource.prototype.$save = function() {
				if (!this.id)
					return this.$create();
				else
					return this.$update
			};

			// Devolucion.
			return resource;
		},

		// Servicio especifico para autenticar.
		auth: function (url, params, methods) {

			// Permite conectarse fuera del dominio.
			//$http.defaults.useXDomains = true;
			delete $http.defaults.headers.common['X-Requested-With'];
			delete $http.defaults.headers.common['X-Parse-Session-Token'];

			$http.defaults.headers.common['X-Parse-Application-Id'] = appOptions.appId;
			$http.defaults.headers.common['X-Parse-REST-API-Key'] = appOptions.apiKey;
			$http.defaults.headers.common['X-Parse-Revocable-Session'] = 1;

			// Extiende los metodos si es necesario.
			var defaultMethods = {
				login: 		{ method: 'GET' },
				logout: 	{ method: 'DELETE' },
				create: 	{ method: 'POST' },
				restorepwd: { method: 'GET' },
				restore: 	{ method: 'POST' }
			};
			methods = angular.extend(defaultMethods, methods);

			// Extiende los parametros de entrada si es necesario.
			var defaultParams = {};
			params = angular.extend(defaultParams, params);

			// Objeto de recurso.
			var resource = $resource(appOptions.url+url, params, methods);
			
			// Devolucion.
			return resource;
		},

		// Servicio especifico para cerrar sesión.
		logout: function (url, params, methods) {

			// Permite conectarse fuera del dominio.
			//$http.defaults.useXDomains = true;
			delete $http.defaults.headers.common['X-Requested-With'];

			$http.defaults.headers.common['X-Parse-Application-Id'] = appOptions.appId;
			$http.defaults.headers.common['X-Parse-REST-API-Key'] = appOptions.apiKey;
			$http.defaults.headers.common['X-Parse-Session-Token'] = $rootScope.currentUser.sessionToken;

			// Extiende los metodos si es necesario.
			var defaultMethods = {
				deleteSession: 	{ method: 'POST' }
			};
			methods = angular.extend(defaultMethods, methods);

			// Extiende los parametros de entrada si es necesario.
			var defaultParams = {};
			params = angular.extend(defaultParams, params);

			// Objeto de recurso.
			var resource = $resource(appOptions.url + url, params, methods);
			
			// Devolucion.
			return resource;
		},

		util: function (url, params, methods) {
			// Extiende los metodos si es necesario.
			var defaultMethods = {
				get: { method: 'GET' },
			};
			methods = angular.extend(defaultMethods, methods);

			// Extiende los parametros de entrada si es necesario.
			var defaultParams = {};
			params = angular.extend(defaultParams, params);

			// Objeto de recurso.
			var resource = $resource(appOptions.url+''+url, params, methods);

			// Devolucion.
			return resource;
		}

	};

});