// Provee una serie de funciones utiles.

appControllers.factory('ParseError', ['$window',
	function ($window) {

	var ParseError = {};

	// Colección de los mensajes de error que devuelve Parse
	ParseError.message = function (code) {
		switch (code) {
			case 1:
				return 'InternalServerError';
			break;
			case 100:
				return 'Error de conexión.';
			break;
			case 101:
				return 'Sus datos son incorrectos.';
			break;
			case 102:
				return 'Invalid Query';
			break;
			case 103:
				return 'InvalidClassName';
			break;
			case 104:
				return 'MissingObjectId';
			break;
			case 105:
				return 'InvalidKeyName';
			break;
			case 106:
				return 'InvalidPointer';
			break;
			case 107:
				return 'InvalidJSON';
			break;
			case 108:
				return 'CommandUnavailable';
			break;
			case 109:
				return 'NotInitialized';
			break;
			case 111:
				return 'IncorrectType';
			break;
			case 112:
				return 'InvalidChannelName';
			break;
			case 115:
				return 'PushMisconfigured';
			break;
			case 116:
				return 'ObjectTooLarge';
			break;
			case 119:
				return 'OperationForbidden';
			break;
			case 120:
				return 'CacheMiss';
			break;
			case 121:
				return 'InvalidNestedKey';
			break;
			case 122:
				return 'InvalidFileName';
			break;
			case 123:
				return 'InvalidACL';
			break;
			case 124:
				return 'Timeout';
			break;
			case 125:
				return 'Correo electrónico no válido';
			break;
			case 137:
				return 'DuplicateValue';
			break;
			case 139:
				return 'InvalidRoleName';
			break;
			case 140:
				return 'ExceededQuota';
			break;
			case 141:
				return 'ScriptFailed';
			break;
			case 142:
				return 'ValidationFailed';
			break;
			case 153:
				return 'FileDeleteFailed';
			break;
			case 155:
				return 'RequestLimitExceeded';
			break;
			case 160:
				return 'InvalidEventName';
			break;
			case 200:
				return 'UsernameMissing';
			break;
			case 201:
				return 'PasswordMissing';
			break;
			case 202:
				return 'El nombre de usuario ya existe. Ingrese otro diferente';
			break;
			case 203:
				return 'El correo electrónico ya existe. Ingrese otro diferente';
			break;
			case 204:
				return 'EmailMissing';
			break;
			case 205:
				return 'EmailNotFound';
			break;
			case 206:
				return 'SessionMissing';
			break;
			case 207:
				return 'MustCreateUserThroughSignup';
			break;
			case 208:
				return 'AccountAlreadyLinked';
			break;
			case 209:
				return 'InvalidSessionToken';
			break;
			case 250:
				return 'LinkedIdMissing';
			break;
			case 251:
				return 'InvalidLinkedSession';
			break;
			case 252:
				return 'UnsupportedService';
			break;
			default:
				return 'Error desconocido';
			break;
		}
	};

	return ParseError;
}]);
