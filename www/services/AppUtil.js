// Provee una serie de funciones utiles.

appControllers.factory('AppUtil', function ($window, $state, $rootScope, $http, BatchService, UserService, appOptions, $mdToast, localStorage, $mdDialog ) {

	var AppUtilObj = {};

	// Limpia las cabeceras
	AppUtilObj.clearHeader = function () {
		delete $http.defaults.headers.common['X-Parse-Application-Id'];
		delete $http.defaults.headers.common['X-Parse-REST-API-Key'];
	};

    AppUtilObj.setHeader = function () {
        $http.defaults.headers.common['X-Parse-Application-Id'] = appOptions.appId;
        $http.defaults.headers.common['X-Parse-REST-API-Key'] = appOptions.apiKey;
    };

	AppUtilObj.cleanSession = function () {
        AppUtilObj.logoutUser($rootScope.currentUser.sessionToken);
        $window.localStorage.removeItem('currentUser');
        $rootScope.currentUser = undefined;
    };

	// Visualiza los mensajes de notificación
	AppUtilObj.noty = function (type, message, container, buttons) {
		var icon;
		switch (type) {
			case 'success':
				icon = 'check';
			break;
			case 'error':
				icon = 'exclamation-triangle';
			break;
			case 'info':
				type = 'information';
			break;
			case 'warning':
				icon = 'exclamation-circle';
			break;
			case 'alert,success':
				type = "success";
				buttons = [{
					addClass: 'btn btn-success btn-sm btn-flat',
					text: 'Aceptar',
					onClick: function ($noty) {
						$noty.close();
					}
				}];
			break;
			case 'alert,warning':
				type = "warning";
				buttons = [{
					addClass: 'btn btn-warning btn-sm btn-flat',
					text: 'Aceptar',
					onClick: function ($noty) {
						$noty.close();
					}
				}];
			break;
			case 'alert,error':
				type = "error";
				buttons = [{
					addClass: 'btn btn-error btn-sm btn-flat',
					text: 'Aceptar',
					onClick: function ($noty) {
						$noty.close();
					}
				}];
			break;
			default:
				type = 'notification';
			break;
		}

		if (message.length == 0)
			return;

		$(container).noty({
			text: message,
			type: type,
			timeout: 4000,
			animation: {
		        open: 'animated slideInRight', // Animate.css class names
		        close: 'animated slideOutRight', // Animate.css class names
		        easing: 'swing', // unavailable - no need
		        speed: 100 // unavailable - no need
		    },
		    template: '<div class="noty_message"><i class="fa fa-'+ icon +'"></i> <span class="noty_text"></span><div class="noty_close"></div></div>',
		    buttons: buttons
		});
	};

	// Función que convierte la imagen en formato BLOB para recibirlo en el back-end
    AppUtilObj.base64ToBlob = function (base64Data, contentType) {
        contentType = contentType || '';
        var sliceSize = 1024;
        var byteCharacters = atob(base64Data);
        var bytesLength = byteCharacters.length;
        var slicesCount = Math.ceil(bytesLength / sliceSize);
        var byteArrays = new Array(slicesCount);

        for (var sliceIndex = 0; sliceIndex < slicesCount; ++sliceIndex) {
            var begin = sliceIndex * sliceSize;
            var end = Math.min(begin + sliceSize, bytesLength);

            var bytes = new Array(end - begin);
            for (var offset = begin, i = 0 ; offset < end; ++i, ++offset) {
                bytes[i] = byteCharacters[offset].charCodeAt(0);
            }
            byteArrays[sliceIndex] = new Uint8Array(bytes);
        }
        return new Blob(byteArrays, { type: contentType });
    }

    // Función que convierte una imagen a formData
    AppUtilObj.imgToBase64 = function (src, callback) {
	  	var canvas = document.createElement('canvas'),
        ctx = canvas.getContext('2d'),
        img = new Image();

	    img.onload = function() {
	        canvas.width = img.width;
	        canvas.height = img.height;
	        ctx.drawImage(img, 0, 0, img.width, img.height);
	        callback(canvas.toDataURL());
	    }
	    img.src = src;
	}

    // Función que permite eliminar usuarios de Parse sin confirmación
    AppUtilObj.deleteUser = function (userId) {
        // Modelo
        var data = {};
        data._ApplicationId = appOptions.appId;
        data._ClientVersion = "js1.6.14";
        data._InstallationId = appOptions.installationId;
        data._MasterKey = appOptions.masterKey;
        data.requests = [{method: "DELETE", path: "/simapi/classes/_User/" + userId}];

        // Elimina al usuario
        delete $http.defaults.headers.common['X-Parse-Application-Id'];
		delete $http.defaults.headers.common['X-Parse-REST-API-Key'];
        BatchService.poster(data, function (response) {
        	$http.defaults.headers.common['X-Parse-Application-Id'] = appOptions.appId;
			$http.defaults.headers.common['X-Parse-REST-API-Key'] = appOptions.apiKey;
        });
    }

    // Función que permite actualizar usuarios de Parse sin confirmación
    AppUtilObj.updateUser = function (userId, object, callback) {
        // Modelo
        var data = {};
        data.objectId = userId;
        data._ApplicationId = appOptions.appId;
        data._MasterKey = appOptions.masterKey;
        data._method = "PUT";
        _.extend(data, object);

        // Actualiza al usuario
        delete $http.defaults.headers.common['X-Parse-Application-Id'];
		delete $http.defaults.headers.common['X-Parse-REST-API-Key'];
        UserService.poster(data, function (response) {
        	$http.defaults.headers.common['X-Parse-Application-Id'] = appOptions.appId;
			$http.defaults.headers.common['X-Parse-REST-API-Key'] = appOptions.apiKey;

			callback(response);
        });
    }

    // Función que permite actualizar el password de un usuario de Parse sin confirmación
    AppUtilObj.resetUserPassword = function (userId, password) {
        // Modelo
        var data = {};
        data.objectId = userId;
        data._ApplicationId = appOptions.appId;
        data._ClientVersion = "js1.6.14";
        data._InstallationId = appOptions.installationId;
        data._MasterKey = appOptions.masterKey;
        data._method = "PUT";
        data.password = password;

        // Actualiza el password
        UserService.poster(data, function (response) {});
    }

    // Función que elimina el token de sesion de un determinado usuario cuando la sesion esta activa
    AppUtilObj.deleteUserSession = function (userId) {
        // Modelo
        var data = {};
        data.limit = 1;
        data._ApplicationId = appOptions.appId;
        data._ClientVersion = "js1.6.14";
        data._InstallationId = appOptions.installationId;
        data._MasterKey = appOptions.masterKey;
        data._method = "GET";
        data.where = {
        	user: {
        		__type: "Pointer",
				className: "_User",
				objectId: userId
        	}
        };

        // Obtiene el token de session
        $http({
        	method: 'POST',
        	url: appOptions.url + 'classes/_Session',
        	data: data
        }).then(function (resp) {
        	// Si hay una sesión activa
        	if (resp.data.results.length > 0) {
	        	var sessionToken = resp.data.results[0].sessionToken;
	        	var sessionId = resp.data.results[0].objectId;

	        	AppUtilObj.logoutUser(sessionId);
        	}
        });
    }

    // Función para desloguear a un usuario cuando es registrado por primera vez
    AppUtilObj.logoutNewUser = function (sessionToken) {
		$http({
			method: 'POST',
			url: appOptions.url + 'logout',
			headers: {
				'X-Parse-Application-Id': appOptions.appId,
				'X-Parse-REST-API-Key': appOptions.apiKey,
				'X-Parse-Session-Token': sessionToken
			}
		}).then(function (resp) {
		});
	}


	// Función para eliminar la sesion de un usuario logueado
    AppUtilObj.logoutUser = function (sessionToken) {

        $http({
            method: 'POST',
            url: appOptions.url + 'logout',
            headers: {
                'X-Parse-Application-Id': appOptions.appId,
                'X-Parse-REST-API-Key': appOptions.apiKey,
                'X-Parse-Session-Token': sessionToken
            }
        }).then(function (resp) {
        });
    }


	// Función para crear una contraseña aleatoria
	AppUtilObj.createPassword = function () {
			var password = '';
			var characters 	= 'abcdefghijklmnopqkrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
				characters += '0123456789';
				/*characters += 'ºª!$%&/()=?¿`+´,.-^*;:_<>[]{}–';*/
				for (var i = 0; i < 10; i++) {
					var index = parseInt( Math.random() * characters.length );
					password += characters.charAt(index);
				};
			
			return password;
	};

	AppUtilObj.normalize = function(data) {
	  var from = "ÃÀÁÄÂÈÉËÊÌÍÏÎÒÓÖÔÙÚÜÛãàáäâèéëêìíïîòóöôùúüûÑñÇç", 
	      to   = "AAAAAEEEEIIIIOOOOUUUUaaaaaeeeeiiiioooouuuunncc",
	      mapping = {};
	 
	  for(var i = 0, j = from.length; i < j; i++ )
	      mapping[ from.charAt( i ) ] = to.charAt( i );
	 
	  return function( str ) {
	      var ret = [];
	      for( var i = 0, j = str.length; i < j; i++ ) {
	          var c = str.charAt( i );
	          if( mapping.hasOwnProperty( str.charAt( i ) ) )
	              ret.push( mapping[ c ] );
	          else
	              ret.push( c );
	      }      
	      return ret.join( '' );
	  }
	 
	};

	// Función para las notificaciones toast de Android
	AppUtilObj.showToast = function (toastPosition, message) {
        $mdToast.show({
            controller: 'toastController',
            templateUrl: 'toast.html',
            hideDelay: 2500,
            position: toastPosition,
            locals: {
                displayOption: {
                    title: message
                }
            }
        });
    };


    // Función que muestra el cuadro de diálogo de alerta
    AppUtilObj.showAlertDialog = function (title, message, redirectTo) {
        $mdDialog.show({
            controller: 'DialogController',
            templateUrl: 'confirm-dialog.html',
            locals: {
                displayOption: {
                    title: title,
                    content: message,
                    ok: "Aceptar"
                }
            }
        }).then(function () {
            if (redirectTo)
                $state.go(redirectTo);
        });
    }// End showAlertDialog.


    // Función que muestra el cuadro de diálogo de confirmación
    AppUtilObj.showConfirmDialog = function (title, message, redirectTo) {
        $mdDialog.show({
            controller: 'DialogController',
            templateUrl: 'confirm-dialog.html',
            locals: {
                displayOption: {
                    title: title,
                    content: message,
                    ok: "Aceptar",
                    cancel: 'Cerrar'
                }
            }
        }).then(function () {
            $state.go(redirectTo);

        }, function () {
        	console.log('close');
        });
    }// End showConfirmDialog.



    /**
    * Calcula el total de un campo en un array y devuelve la suma [{a: 1}, {a:1}] => 2
    */
    AppUtilObj.getTotal = function(array, key, key2, key3){

        if (array && !key2 && !key3) {
            var total = 0;
            for(var i = 0; i < array.length; i++){
                if (array[i][key]) {
                    var item = array[i][key];
                    total += item;
                }
            }
            return total;
        }

        if (array && key2  && !key3) {
            var total = 0;
            for(var i = 0; i < array.length; i++){
                if (array[i][key][key2]) {
                    var item = array[i][key][key2];
                    total += item;
                }
            }
            return total;
        }

        if (array && key2  && key3) {
            var total = 0;
            for(var i = 0; i < array.length; i++){
                if (array[i][key][key2][key3]) {
                    var item = array[i][key][key2][key3];
                    total += item;
                }
            }
            return total;
        }
    };


    /**
    * Establece el tamaño cover y centrado de la imagen de portada
    */
    AppUtilObj.setCoverImage = function () {
        var img = document.getElementById("image-cover");
        var width = img.clientWidth;
        var height = img.clientHeight;

        var imgClass = (width/height > 1) ? 'wide' : 'tall';

        img.className += " " + imgClass;
    }


    /**
    * Función para mandar notificación
    */
    AppUtilObj.send_notification = function (data) {

        delete $http.defaults.headers.common['X-Parse-Application-Id'];
        delete $http.defaults.headers.common['X-Parse-REST-API-Key'];
        delete $http.defaults.headers.common['X-Parse-Revocable-Session'];
        $http.post('http://www.prowebmerida.com/menudigitalapp/app/includes/notification.php', {
            appId: appOptions.appId,
            data: data
        }).then(function (success) {
            $http.defaults.headers.common['X-Parse-Application-Id'] = appOptions.appId;
            $http.defaults.headers.common['X-Parse-REST-API-Key'] = appOptions.apiKey;
        },
        function (error) {});
    }

    
    /**
    * Función para enviar correo de recuperación de contraseña
    * params
    * data: parámetros requeridos para el envío de correo (correo, password, nombre, apellido, target='reset_password')
    */
    AppUtilObj.send_mail = function (data, success) {
        delete $http.defaults.headers.common['X-Parse-Application-Id'];
        delete $http.defaults.headers.common['X-Parse-REST-API-Key'];
        delete $http.defaults.headers.common['X-Parse-Revocable-Session'];

        _.extend(data, {appId: appOptions.appId});

        $http.post(appOptions.externalUrl + 'includes/sendMail.php', {
            appId: appOptions.appId,
            data: data
        }).then(function (resp) {

            $http.defaults.headers.common['X-Parse-Application-Id'] = appOptions.appId;
            $http.defaults.headers.common['X-Parse-REST-API-Key'] = appOptions.apiKey;

            if (success) success(resp);
            
        }, function (error) {});
    }


    /**
    * Función para eliminar archivos del servidor
    */
    AppUtilObj.deleteFile = function (filename, directory, type, success, error) {
        $http({
            method: 'POST',
            url: '../includes/deleteFile.php',
            data: {
                directory: '../' + directory, 
                filename: filename, 
                type: type
            }
        }, function (resp) {
            if (success) return success(resp);

        }, function (e) {
            if (error) return error(e);
        });
    }


    /**
    * Convierte un texto en slug
    * Params
    * text: string
    */
    AppUtilObj.slugify = function (text) {
        return text.toString().toLowerCase()
            .replace(/\s+/g, '-')           // Replace spaces with -
            .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
            .replace(/\-\-+/g, '-')         // Replace multiple - with single -
            .replace(/^-+/, '')             // Trim - from start of text
            .replace(/-+$/, '');            // Trim - from end of text
    }

	return AppUtilObj;
});
