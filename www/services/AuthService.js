'use strict';

appControllers.factory('AuthService', ['ResourceService', function (ResourceService) {
	return {
		auth: function () {
			return ResourceService.auth('login', {});
		},
		logout: function () {
			return ResourceService.logout('logout', {});
		},
		signup: function () {
			return ResourceService.auth('signup', {});
		},
		forgot: function () {
			return ResourceService.auth('restorepwd/:email', {});
		},
		restore: function () {
			return ResourceService.auth('restore', {});
		}
	};
}]);