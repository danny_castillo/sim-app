'use strict';

appControllers.factory('BatchService', ['ResourceService', function (ResourceService) {
    
    return ResourceService.call('batch/:objectId', {
    	objectId: '@objectId'
    });

}]);
