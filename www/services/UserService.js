'use strict';

appControllers.factory('UserService', ['ResourceService', function (ResourceService) {
    
    return ResourceService.call('classes/_User/:objectId', {
        objectId: '@objectId'
    });

}]);
