'use strict';

appControllers.factory('RestService', ['ResourceService', function (ResourceService) {
    
    return ResourceService.call('classes/:clase/:objectId', {
        objectId: '@objectId',
        clase: '@clase'
    });

}]);
